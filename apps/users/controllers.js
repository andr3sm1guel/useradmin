'use strict';
const express = require('express'),
	  router = express.Router(),
	  User = require('./models').User;


router.route('/ingresar/')

	.get(function (req, res) {
		return res.render('login2.html');
	})
	.post(function (req,res){
		User.find({
			email: req.body.username,password: req.body.password,
		},function(err,user){
			if(!err && req.body.username == 'admin' && req.body.password == 'admin' ) {
				res.redirect('/admin/usuarios/')
			}else res.render('users/panel.html')
		})
	});

	router.route('/admin/usuarios/crear')

		.get(function (req, res) {
			return res.render('registrar.html');
		})
		.post(function (req, res) {
			let user = new User({
				username: req.body.username,
				password: req.body.password,
				email: req.body.emal,
			});
			user.save(function (err) {
				if (!err) return res.redirect('/admin/usuarios/');
			})
		});

router.route('/salir/')

	.get(function (req, res) {
		req.logout();
		res.redirect('main/home.html');
	});

router.route('/admin/usuarios/')

	.get(function (req, res) {
		User.find()
		.then(function (users) {
			res.locals.users = users;
			return res.render('users/users.html');
		});
	});



module.exports = router;
