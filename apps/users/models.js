'use strict';
const mongoose = require('../../config/mongoose'),
	  userSchema = require('./schemas').userSchema;

const models = {

	User: mongoose.model('User', userSchema)

};

module.exports = models;
